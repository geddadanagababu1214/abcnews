import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  emailPattern ="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  signUpform = new FormGroup({
    userName: new FormControl('', Validators.required),
    emailId : new FormControl('',Validators.pattern(this.emailPattern)),
    password: new FormControl('',[Validators.required,Validators.minLength(5),Validators.maxLength(8)]),
    conformPassword: new FormControl('',Validators.required)
  })
  constructor(private route: Router) { }

  ngOnInit(): void {
  }

  submit(){
    console.log(this.signUpform.value)
  }

  get function():{
    [key:string] : AbstractControl
  } {
    return this.signUpform.controls
  }
newMethod(){
  console.log('rrr');
  
}
}
