import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { NewsComponent } from './news/news.component';
import { NationalnewsComponent } from './nationalnews/nationalnews.component';
import { StatenewsComponent } from './statenews/statenews.component';
import { SportsnewsComponent } from './sportsnews/sportsnews.component';
import { EntertainmentnewsComponent } from './entertainmentnews/entertainmentnews.component';
import { LoginComponent } from './login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LatestNewsComponent } from './latest-news/latest-news.component';
import { SignUpComponent } from './sign-up/sign-up.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NewsComponent,
    NationalnewsComponent,
    StatenewsComponent,
    SportsnewsComponent,
    EntertainmentnewsComponent,
    LoginComponent,
    LatestNewsComponent,
    SignUpComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
