import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-latest-news',
  templateUrl: './latest-news.component.html',
  styleUrls: ['./latest-news.component.css']
})
export class LatestNewsComponent implements OnInit {

  mostViews = [
    {
      imgUrl :"https://assets-news-bcdn.dailyhunt.in/cmd/resize/650x350_90/fetchdata16/images/62/31/a7/6231a7549a48ca10368d772b1dd902f30f8049877e963ed87cecc53d057645fa.webp",
      headline :"భారత్‌కు థ్యాంక్స్ చెప్పిన ఉక్రెయిన్",
      content:"కీవ్: భారత ప్రధానమంత్రి నరేంద్రమోదీకి ఉక్రెయిన్ అధ్యక్షుడు వ్లాదిమిర్ జెలెన్‌స్కీ కృతజ్ఞతలు తెలిపింది. రష్యా సేనలను ఉక్రెయిన్",
      date:new Date(),
    }]

  subViews = [
    {
      imgUrl :"https://assets-news-bcdn.dailyhunt.in/cmd/resize/1000x600_90/fetchdata16/images/40/30/d2/4030d2c4405e2556d7f3cdc9858c97cdce4858b6947599c6eadd7e5af00892ef.webp",
      headline :"రష్యా అధ్యక్షుడు పుతిన్‌తో 50 నిమిషాలు ఫోన్‌లో మాట్లాడిన మోడీ",
      content:"ఇంకా ఉక్రెయిన్‌లో ఉన్న భారతీయుల తరలింపునకు సహకరించాలన్నారు. అదే సమయంలో ఉక్రెయిన్ అధ్యక్షుడితోనూ నేరుగా చర్చలు జరపాలంటూ పుతిన్‌ను మోడీ విజ్ఞప్తి చేశారు. అలా చేయడం ద్వారా సమస్యకు పరిష్కారం దొరుకుతుందని ఆశాభావం వ్యక్తం చేశారు. కాల్పుల విరమణ ప్రకటించి భారతీయుల తరలింపునకు సహకరిస్తున్నందుకు రష్యాకు మోడీ ధన్యవాదాలు తెలిపారు.",
      date:new Date(),
    },

    {
      imgUrl :"https://assets-news-bcdn.dailyhunt.in/cmd/resize/850x500_90/fetchdata16/images/d8/33/36/d8333654e698c6891108079780af690bafe13f56ab37f1953119db0924fe3c8a.webp",
      headline :"తాత్కాలిక కాల్పుల విరమణ ప్రకటించిన రష్యా",
      content:"గత పది రోజులకు పైగా ఉక్రెయిన్ – రష్యా మధ్య యుద్ధం జరుగుతోంది. ఉక్రెయిన్‌పై కాల్పులు, బాంబులతో విరుచుకుపడుతోన్న రష్యా తాత్కాలికంగా కాల్పుల విరమణ ప్రకటించింది.",
      date:new Date(),
    },

    {
      imgUrl :"https://assets-news-bcdn.dailyhunt.in/cmd/resize/650x400_90/fetchdata16/images/c9/ca/19/c9ca1997d68751fbfb48a8ddfe295fc7cb6ad806071ff13b0f047c0589aaa550.webp",
      headline :"కన్ఫ్యూజన్‌లో మధ్యవర్తిని కాల్చి చంపిన ఉక్రెయిన్‌",
      content:"కీవ్‌: ఉక్రెయిన్‌ తరఫున శాంతి చర్చల్లో పాల్గొన్న డెనిస్‌ కిరీవ్‌ని ఆ దేశ సీక్రెట్‌ సర్వీస్‌ కాల్చి చంపింది. కిరీవ్‌ ఒక గూఢచారి అని, అరెస్టుకు సహకరించకపోవడంతో చంపాల్సి వచ్చిందని పార్లమెంట్‌ సభ్యులు తెలిపారు.",
      date:new Date(),
    },

    {
      imgUrl :"https://assets-news-bcdn.dailyhunt.in/cmd/resize/750x450_90/fetchdata16/images/7f/89/b2/7f89b28e49be1652f5443b673a9903c541a829d6c59a6e9f720133de1a70369d.webp",
      headline :"అమెరికా జెట్‌లపై చైనా జెండాలు అమర్చి. రష్యాపై బాంబులు వేయండి: ట్రంప్",
      content:"వాషింగ్టన్‌: ఉక్రెయిన్‌పై రష్యా యుద్ధం నేపథ్యంలో అమెరికా మాజీ అధ్యక్షుడు డోనాల్డ్ ట్రంప్ మరోసారి సంచలన వ్యాఖ్యలు చేశారు. అమెరికాకు చెందిన ఎఫ్‌-22 యుద్ధ విమానాలపై చైనా జెండాలు అమర్చి రష్యాపై బాంబులు వేయాలని అన్నారు.",
      date:new Date(),
    },
  ]

  
  constructor() { }

  ngOnInit(): void {
  }

}
