import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StatenewsComponent } from './statenews.component';

describe('StatenewsComponent', () => {
  let component: StatenewsComponent;
  let fixture: ComponentFixture<StatenewsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StatenewsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StatenewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
