import { NgModule } from '@angular/core';
import { Router, RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { EntertainmentnewsComponent } from './entertainmentnews/entertainmentnews.component';
import { HomeComponent } from './home/home.component';
import { LatestNewsComponent } from './latest-news/latest-news.component';
import { LoginComponent } from './login/login.component';
import { NationalnewsComponent } from './nationalnews/nationalnews.component';
import { NewsComponent } from './news/news.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { SportsnewsComponent } from './sportsnews/sportsnews.component';
import { StatenewsComponent } from './statenews/statenews.component';

const routes: Routes = [
  {path:'',component:HomeComponent},
  {path:'home',component:HomeComponent},
  {path:'entertainmentnews',component:EntertainmentnewsComponent},
  {path:'login',component:LoginComponent},
  {path:'nationalnews',component:NationalnewsComponent},
  {path:'news',component:NewsComponent},
  {path:'sign-up',component:SignUpComponent},
  {path:'sportsnews',component:SportsnewsComponent},
  {path:'statenews',component:StatenewsComponent},
  {path:'latest-news',component:LatestNewsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { 
}
